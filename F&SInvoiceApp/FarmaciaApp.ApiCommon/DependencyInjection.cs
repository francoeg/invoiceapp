﻿using Microsoft.Extensions.DependencyInjection;

namespace FarmaciaApp.ApiCommon
{
    public static  class DependencyInjection
    {
        public static IServiceCollection AddApiCommonServices(
                               this IServiceCollection services)
        {
            //services.AddScoped<Presentation>();
            return services;
        }
    }
}
