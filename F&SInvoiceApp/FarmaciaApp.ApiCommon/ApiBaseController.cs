﻿using FarmaciaApp.ApiCommon;
using FarmaciaApp.Common.Commands.Mediator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FarmaciaApp.Api.Controllers
{
    [ApiController]
   
    public abstract class ApiBaseController : ControllerBase
    {

        protected readonly IEventMediator EventMediator;
        protected readonly Presenter Presenter;

        public ApiBaseController(IEventMediator eventMediator, Presenter presenter)
        {
            EventMediator = eventMediator;
            Presenter = presenter;
        }
    }
}
