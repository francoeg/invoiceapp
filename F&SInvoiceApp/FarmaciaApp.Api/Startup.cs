using AutoMapper;
using Commom.Application;

using FarmaciaApp.Api.Common;
using FarmaciaApp.Api.Services;
using FarmaciaApp.Application.Common.Interfaces;
using FarmaciaApp.Infrastructure;
using FarmaciaApp.Infrastructure.Data;
using Invoice.Application;
using InvoiceApp.Common.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using User.Application;

namespace Inovice.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddControllersWithViews();
           //Todo: Pasar a metodo y una clase  mas prolija
            //DependencyInjection capas
            services.AddApplication();
            services.AddUserApplication();
            services.AddApplicationCommon();
            services.AddInfrastructure(Configuration);
            //DependencyInjection            
            services.AddTransient<ICurrentUserService, CurrentUserService>();
            services.AddSingleton<Presentation.Presentation>();
            services.AddTransient<IDbContext, ApplicationDbContext>();


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Invoice.Api", Version = "v1" });
                string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            //services.AddMediatR(typeof(InsertUserRequest).GetTypeInfo().Assembly,
            //                   typeof(SettingInsertRequest).GetTypeInfo().Assembly);
        
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            //app.UseCookiePolicy();
            app.UseCors(
                 options => options.SetIsOriginAllowed(x => _ = true).
                        AllowAnyMethod().AllowAnyHeader().AllowCredentials()
             );
            app.UseRouting();
            app.UseAuthorization();
            app.UseCustomExceptionHandler();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "FarmaciaApp.Api V1");

            });
      


        }
    }
}
