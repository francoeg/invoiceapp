﻿using Commom.Application.Commands.Settting.Command.InsertCommand;
using Commom.Application.Commands.Settting.Queries.GetById;
using FarmaciaApp.Api.Controllers;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using Inovice.Api.Presentation;
using InvoiceApp.Common.ModelsDto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace InvoiceApp.Api.Controllers
{
    [Route("api/[controller]")]

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SettingController : ApiBaseController
    {
     

        public SettingController(IEventMediator eventMediator,
                                 Presentation presentation) 
                          : base(eventMediator, presentation)
        {
            
        }

        /// <summary>
        /// Agregar Setting
        /// </summary>
        /// <param Setting="item"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]SettingDto item)
        {
            EntityResult<SettingDto> result = await EventMediator.Send
                                                   (new SettingInsertRequest(item));

            return Presenter.GetCreatedResult(result, Request, $"{Request.Path.Value}/{item.Id}");
        }

        /// <summary>
        /// Busca la setting por id
        /// </summary>
        /// <param setting="settingGetById"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("setting/getid")]
        public async Task<IActionResult> GetSettingById([FromBody] SettingDto settingGetById)
        {
            EntityResult<SettingDto> result =
           await EventMediator.Send(new GetSettingByIdRequest(settingGetById));
            return Presenter.GetCreatedResult(result, Request,
            $"{Request.Path.Value}");
        }
    }
}
