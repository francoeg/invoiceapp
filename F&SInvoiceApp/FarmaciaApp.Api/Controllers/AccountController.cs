﻿using FarmaciaApp.ApiCommon;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using FarmaciaApp.Common.Models.Result;
using Inovice.Api.Presentation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using User.Application.EntitiesVM;
using User.Application.User.Command.DeleteCommand;
using User.Application.User.Command.InsertCommand;
using User.Application.User.Queries.GetAll;
using User.Application.User.Queries.GetById;
using User.Application.UserLogin.Command.Login;

namespace FarmaciaApp.Api.Controllers
{
    [Route("api/[controller]")]

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AccountController : ApiBaseController
    {

        public AccountController(IEventMediator eventMediator,
                                 Presentation presentation)
                          : base(eventMediator,
                                 presentation)
        {
        }

        /// <summary>
        /// Registrar nuevo usuario (ho revisams proceso de logistica)
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]UserVM item)
        {
            EntityResult<UserVM> result =await EventMediator.Send(new InsertUserRequest(item));

            return Presenter.GetCreatedResult(result, Request, $"{Request.Path.Value}/{item.Id}");
        }
        /// <summary>
        ///   devolver usuario autenticado
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("token")]
        public async Task<IActionResult> Token([FromBody]UserLoginVM userLogin)
        {
            EntityResult<UserTokenVm> result =
           await EventMediator.Send(new UserLoginRequest(userLogin));
            return Presenter.GetCreatedResult(result, Request,
            $"{Request.Path.Value}");
        }
        /// <summary>
        /// Busca el usuario por id
        /// </summary>
        /// <param name="userGetById"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("user/getid")]
        public async Task<IActionResult> GetUserById([FromBody]UserGetByIdVm userGetById)
        {
            EntityResult<UserVM> result =
           await EventMediator.Send(new GetUserByIdRequest(userGetById));
            return Presenter.GetCreatedResult(result, Request,
            $"{Request.Path.Value}");
        }

        /// <summary>
        /// Busca los usuarios
        /// </summary>
        /// <param name="userFindVm"></param>
        /// <returns></returns>
       
        [HttpPost("user/find")]
        public async Task<IActionResult> GetUsers([FromBody]UserFindVm userFindVm)
        {
            QueryResult<UserVM> result =
           await EventMediator.Send(new FindUserRequest(userFindVm,                                         userFindVm.PageIndex, userFindVm.PageSize));
            return Presenter.GetListResult(Response, result);
        }

        [HttpPost("user/Delete/{Id}")]
        [AllowAnonymous]
        public async Task<IActionResult> DeleteUsers(string Id)
        {
          Result result =
           await EventMediator.Send(new DeleteUserRequest(Id));
            return Presenter.GetOkResult( result);
        }
    }
}

