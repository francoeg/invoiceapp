﻿using FarmaciaApp.Api.Controllers;
using FarmaciaApp.ApiCommon;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using Inovice.Api.Presentation;
using InvoiceApp.Common.ModelsView;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using User.Application.EntitiesVM.Find;
using User.Application.ModelDto;
using User.Application.Roles.Command.InsertCommnad;
using User.Application.Roles.Queries.GetAll;

namespace InvoiceApp.Api.Controllers
{
    [Route("api/[controller]")]

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RoleController : ApiBaseController
    {

        public RoleController(IEventMediator eventMediator,
                              Presentation presentation) 
                       : base(eventMediator, presentation)
        {
        }

        /// <summary>
        /// Agregar Role
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<IActionResult> Register([FromBody]InputModel<string> item)
        {
            EntityResult<RoleDto> result = await EventMediator.Send
                                                   (new RoleInsertRequest(new RoleDto                                                                   ( item.Name)));

            return Presenter.GetCreatedResult(result, Request, $"{Request.Path.Value}/{item.Id}");
        }

        /// <summary>
        /// Busca los Role
        /// </summary>
        /// <param name="roleFindVm"></param>
        /// <returns></returns>

        [HttpPost("find")]
        [AllowAnonymous]
        public async Task<IActionResult> GetUsers([FromBody]RoleFindVM roleFindVm)
        {
            QueryResult<RoleDto> result =
           await EventMediator.Send(new RoleFindRequest(roleFindVm, roleFindVm.PageIndex, roleFindVm.PageSize));
            return Presenter.GetListResult(Response, result);
        }

    }
}
