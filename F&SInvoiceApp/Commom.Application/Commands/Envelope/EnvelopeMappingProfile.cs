﻿using AutoMapper;
using Commom.Application.ModelDto;

namespace Commom.Application.Commands.Envelope
{
    public class EnvelopeMappingProfile : Profile
    {
        public EnvelopeMappingProfile()
        {
            CreateMap<EnvelopeDto, Invoice.Domain.Entities.Envelope>();

            CreateMap<Invoice.Domain.Entities.Envelope, EnvelopeDto>();
        }
    }
}
