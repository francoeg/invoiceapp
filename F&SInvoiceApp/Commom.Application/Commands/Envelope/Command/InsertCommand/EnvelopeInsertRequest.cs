﻿using Commom.Application.ModelDto;
using FarmaciaApp.Common.Models.Request;

namespace Commom.Application.Commands.Envelope.Command.InsertCommand
{
    public class EnvelopeInsertRequest : EntityRequest<EnvelopeDto>
    {
        public EnvelopeInsertRequest( EnvelopeDto item): base(item)
        {
            AddNotificationsValidation(new EnvelopeRequestValidators().Validate(this));
        }
    }
}
