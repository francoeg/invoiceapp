﻿using AutoMapper;
using Commom.Application.ModelDto;
using FarmaciaApp.Common.Commands.BaseRequest;
using FarmaciaApp.Common.Commands.Mediator;
using InvoiceApp.Common.Interface;
using System.Threading;
using System.Threading.Tasks;
using Invoice.Domain.Entities;
using FarmaciaApp.Common.Models;

namespace Commom.Application.Commands.Envelope.Command.InsertCommand
{
    public class EnvelopeInsertHandler : BaseRequestHandler<EnvelopeInsertRequest, EntityResult<EnvelopeDto>>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;

        public EnvelopeInsertHandler(IEventMediator eventPublisher,
                                    IDbContext dbContext,
                                    IMapper mapper) : base(eventPublisher)
        {
            this._dbContext = dbContext;
            this._mapper = mapper;
        }

        public override async  Task<EntityResult< EnvelopeDto>> Handle(EnvelopeInsertRequest request, CancellationToken cancellationToken)
        {

            Invoice.Domain.Entities.Envelope envelope = this._mapper.Map<Invoice.Domain.Entities.Envelope>(request.Item);

            await this._dbContext.Envelopes.AddAsync(envelope);

            await this._dbContext.SaveChangesAsync(cancellationToken);

            return GenerateEntityResult<EnvelopeDto>(request.Notifications, request.Item);

        }
    }
}
