﻿
using FluentValidation;

namespace Commom.Application.Commands.Envelope.Command.InsertCommand
{
    public class EnvelopeRequestValidators : AbstractValidator<EnvelopeInsertRequest>
    {
        public EnvelopeRequestValidators()
        {
            RuleFor(x => x.Item).NotNull().WithMessage("La entidad no puede ser vacia");
            RuleFor(x => x.Item.Title).NotEmpty();
            RuleFor(x => x.Item.Header).NotEmpty();
            RuleFor(x => x.Item.Details).NotEmpty();
            RuleFor(x => x.Item.Footer).NotEmpty();
        }
    }
}
