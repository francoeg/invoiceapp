﻿using System;
using System.Collections.Generic;
using System.Text;
using InvoiceApp.Common.ModelsDto;
using FarmaciaApp.Common.Models.Request;
using FarmaciaApp.Common.Validation;

namespace Commom.Application.Commands.Settting.Queries.GetById
{
    public class GetSettingByIdRequest : EntityRequest<SettingDto>
    {
        public GetSettingByIdRequest(SettingDto SettingGetByIdVm) :base(SettingGetByIdVm)
        {
            SettingGetById = SettingGetByIdVm;

            AddNotifications(new NotEmptyValidator<string>().Validate(SettingGetById.Id.ToString()));
        }
        public SettingDto SettingGetById { get; }
    }
}
