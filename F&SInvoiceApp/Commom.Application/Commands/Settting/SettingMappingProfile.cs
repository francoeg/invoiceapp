﻿using AutoMapper;
using Invoice.Domain.Entities;
using InvoiceApp.Common.ModelsDto;

namespace Commom.Application.Commands.Settting
{
    public class SettingMappingProfile : Profile
    {
        public SettingMappingProfile()
        {
            CreateMap<SettingDto, Setting>();

            CreateMap<Setting, SettingDto>();
        }
    }
}
