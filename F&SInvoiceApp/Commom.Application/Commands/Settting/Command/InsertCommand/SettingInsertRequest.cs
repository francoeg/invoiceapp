﻿
using FarmaciaApp.Common.Models.Request;
using InvoiceApp.Common.ModelsDto;

namespace Commom.Application.Commands.Settting.Command.InsertCommand
{
    public class SettingInsertRequest : EntityRequest<SettingDto>
    {
        public SettingInsertRequest(SettingDto item) : base (item)
        {
            this.Item = item;           
        }
    }
}
