﻿
using FluentValidation;

namespace Commom.Application.Commands.Settting.Command.InsertCommand
{
    public class SettingInsertRequestValidators : AbstractValidator<SettingInsertRequest>
    {
        public SettingInsertRequestValidators()
        {
            RuleFor(x => x.Item.Name).NotEmpty().
                                    WithMessage("El Nombre de la setting no puede ser nulo.")
                                    .MinimumLength(2);
            RuleFor(x => x.Item.Value).NotEmpty().
                                    WithMessage("El Valor de la setting no puede ser nulo.");

            //RuleFor(x => x.Item.Value).NotEmpty().
            //                        WithMessage("El Nombre de la setting no puede ser nulo");

        }
    }
}
