﻿using AutoMapper;
using FarmaciaApp.Common.Commands.BaseRequest;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using Invoice.Domain.Entities;
using InvoiceApp.Common.Interface;
using InvoiceApp.Common.ModelsDto;
using System.Threading;
using System.Threading.Tasks;


namespace Commom.Application.Commands.Settting.Command.InsertCommand
{
    public class SettingInsertHandler : BaseRequestHandler<SettingInsertRequest,                                                                    EntityResult<SettingDto>>

    {
        private readonly IMapper _mapper;
        private readonly IDbContext _dbContext;

        public SettingInsertHandler(IMapper mapper,
                                    IDbContext dbContext ,
                                    IEventMediator eventPublisher) : base(eventPublisher)
        {
            this._mapper = mapper;
           this._dbContext = dbContext;
        }


        public override async Task<EntityResult<SettingDto>> Handle(SettingInsertRequest request, CancellationToken cancellationToken)
        {
            Setting setting = this._mapper.Map<Setting>(request.Item);

            await this._dbContext.Settings.AddAsync(setting);

            await this._dbContext.SaveChangesAsync(cancellationToken);

            return GenerateEntityResult<SettingDto>(request.Notifications,request.Item);   
        }
    }
}
