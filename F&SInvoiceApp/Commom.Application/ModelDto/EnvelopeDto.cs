﻿namespace Commom.Application.ModelDto
{
    public class EnvelopeDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Header { get; set; }

        public string Footer { get; set; }

        public string Details { get; set; }
    }
}
