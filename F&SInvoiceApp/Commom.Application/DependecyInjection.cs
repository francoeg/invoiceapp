﻿using Commom.Application.Commands.Settting.Command.InsertCommand;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Commom.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplicationCommon(this IServiceCollection services)
        {
            services.AddMediatR(
                        typeof(SettingInsertRequest).GetTypeInfo().Assembly);
            return services;
        }
    }
}
