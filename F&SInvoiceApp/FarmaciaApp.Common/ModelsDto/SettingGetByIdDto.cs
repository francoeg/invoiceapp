﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InvoiceApp.Common.ModelsDto
{
    public class SettingGetByIdDto
    {
        public int Id { get; }
        public string Name { get; }
        public string Value { get; }
        public double ValueNumber { get; }

    }
}
