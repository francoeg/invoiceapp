﻿using System;

namespace FarmaciaApp.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
