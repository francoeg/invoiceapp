﻿namespace InvoiceApp.Common.ExtensionMethods
{
    public static class Extensionsvalidations
    {
        public static object NotNull(this object obj, string name)
        {
            if (obj == null || obj.Equals(false))
            {
                throw new System.ArgumentException("Object '" + name.ToString() + "' is null.", obj.GetType().ToString());
            }
            return true;
        }

        public static object IsNull(this object obj, string name)
        {
            if (obj == null || obj.Equals(false))
            {
                return true;
            }
            return false;
        }

        public static bool IsNullOrWhiteSpace(this object obj)
        {
            if (obj == null || obj.Equals(false) || obj.ToString() == "")
            {
                return true;
            }
            return false;
        }
    }
}
