﻿
using Invoice.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace InvoiceApp.Common.Interface
{
    public interface IDbContext
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken());

        public DbSet<Setting> Settings { get; set; }

        public DbSet<Envelope> Envelopes { get; set; }
    }
}
