﻿using FluentValidation.Results;
using System.Collections.Generic;


namespace FarmaciaApp.Common.Models
{
    public class EntityResult<T> : Result.Result
        where T : class
    {
        public EntityResult(IReadOnlyCollection<ValidationResult> notifications, T item) :
            base(notifications)
        {
            Item = item;
        }

        public T Item { get; }
   }
}
