﻿using FluentValidation.Results;
using System.Collections.Generic;


namespace FarmaciaApp.Common.Models
{
    public class QueryResult<T> : Result.Result
      where T : class
    {
        public QueryResult(IReadOnlyCollection<ValidationResult> notifications,
            IEnumerable<T> items,
            long count) : base(
            notifications)
        {
            Items = items;
            Count = count;
        }

        public IEnumerable<T> Items { get; }

        public long Count { get; }
    }
}
