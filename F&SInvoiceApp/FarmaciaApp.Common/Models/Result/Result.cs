﻿using FluentValidation.Results;
using System.Collections.Generic;
using System.Linq;

namespace FarmaciaApp.Common.Models.Result
{
    public class Result
    {
        
        public Result(IReadOnlyCollection<ValidationResult> notifications)
        {
            Notifications = notifications;
        }

        public IReadOnlyCollection<ValidationResult> Notifications { get; }

        public bool IsValid => Notifications.Count == 0
                    || Notifications.All(x => x.IsValid);

      
    }
}
