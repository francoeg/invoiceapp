﻿namespace FarmaciaApp.Common.Models.Request
{
    public abstract class EntityRequest<T> : Request<EntityResult<T>>
         where T : class
    {
        protected EntityRequest( T item )
        {
            this.Item = item;
        }

        public T Item { get; set; }
    }
}
