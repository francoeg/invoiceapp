﻿using InvoiceApp.Common.ExtensionMethods;

namespace FarmaciaApp.Common.Models.Request
{
    public abstract class QueryRequest<T> : Request<QueryResult<T>>
      where T : class
    {
        protected QueryRequest(int pageIndex = 1, int pageSize = 200)
        {
            PageIndex = SetPageIndex(pageIndex);
            PageSize = SetPageSize(pageSize);
        }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }


       private int SetPageIndex(int pageIndex) 
        {
            if (PageIndex == 0) return 1;

            return pageIndex;
        }

        private int SetPageSize(int pageSize)
        {
            if (pageSize == 0) return 200;

            return pageSize;
        }
    }
}
