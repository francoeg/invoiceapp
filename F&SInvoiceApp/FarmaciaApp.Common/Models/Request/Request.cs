﻿using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;
using MediatR;

using FarmaciaApp.Application.Common.Exceptions;

namespace FarmaciaApp.Common.Models.Request
{
    public abstract class Request<T> : IRequest<T>
        where T : Result.Result
    {
        private readonly List<ValidationFailure> failures;

        protected Request()
        {
            Notifications = new List<ValidationResult>();
            failures = new List<ValidationFailure>();
        }

        public bool IsValid => Notifications.All(x => x.IsValid);

        public List<ValidationResult> Notifications { get; }

        public void AddNotifications(params ValidationResult[] items)
        {
            Notifications.AddRange(items);
        }

        public void AddNotificationsValidation(params ValidationResult[] items)
        {
            Notifications.AddRange(items);
            ValidateRequest();
        }

        private void ValidateRequest() 
        {
            if (this.IsValid) return;
               
            foreach (ValidationResult notification in this.Notifications)
            {
                  failures.AddRange(notification.Errors.ToList());                   
            }
              
            throw new ValidationException(failures);
         }
    } 
}
