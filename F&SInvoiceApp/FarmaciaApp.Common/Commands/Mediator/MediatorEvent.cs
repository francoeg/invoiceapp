﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace FarmaciaApp.Common.Commands.Mediator
{
    public class MediatorEvent : IEventMediator
    {
        private readonly IMediator _mediator;

        public MediatorEvent(IMediator mediator)
        {
            _mediator = mediator;
        }

        public Task Publish<TNotification>(TNotification notification,
            CancellationToken cancellationToken = default)
            where TNotification : INotification
        {
            return _mediator.Publish(notification, cancellationToken);
        }

        public Task<TResponse> Send<TResponse>(IRequest<TResponse> request,
            CancellationToken cancellationToken = default)
        {
            return _mediator.Send(request, cancellationToken);
        }
    }
}
