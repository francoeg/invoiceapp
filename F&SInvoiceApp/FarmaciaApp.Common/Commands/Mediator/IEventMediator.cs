﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace FarmaciaApp.Common.Commands.Mediator
{
    public interface IEventMediator
    {
        Task Publish<TNotification>(TNotification notification,
          CancellationToken cancellationToken = default)
          where TNotification : INotification;

        Task<TResponse> Send<TResponse>(IRequest<TResponse> request,
            CancellationToken cancellationToken = default);
    }
}
