﻿
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using FarmaciaApp.Common.Validation;
using FluentValidation.Results;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FarmaciaApp.Common.Commands.BaseRequest
{
    public abstract class
         BaseRequestHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse>
         where TRequest : IRequest<TResponse>
    {
        protected readonly IEventMediator EventPublisher;

        protected BaseRequestHandler(IEventMediator eventPublisher)
        {
            EventPublisher = eventPublisher;
        }

        public abstract Task<TResponse> Handle(TRequest request,
            CancellationToken cancellationToken);

        protected ValidationResult NotEmpty<T>(T item)
        {
            return new NotEmptyValidator<T>().Validate(item);
        }

        protected static ValidationResult NotNull<T>(T item)
        {
            return new NotNullValidator<T>().Validate(item);
        }

        protected static ValidationResult Exists<T>(T item)
           where T : class
        {
            ValidationResult res = new ValidationResult();
            if (item == null)
            {
                ValidationFailure failure =
                    new ValidationFailure("NotFound", "Item was not found.");
                res.Errors.Add(failure);
            }

            return res;
        }

        protected static ValidationResult Error(string Error)
        {
            ValidationResult res = new ValidationResult();
            res.Errors.Add(new ValidationFailure("NotFound", Error));
            return res;
        }
       
        protected EntityResult<T> GenerateEntityResult<T>(IReadOnlyCollection<ValidationResult> notifications, T item)
             where T : class
        {
            return new EntityResult<T>(notifications, item);
        }
    }
}
