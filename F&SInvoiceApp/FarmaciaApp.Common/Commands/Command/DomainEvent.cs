﻿using MediatR;
using System;

namespace FarmaciaApp.Common.Commands.Command
{
    public abstract class DomainEvent : INotification
    {
        protected DomainEvent()
        {
            CreatedAt = DateTime.UtcNow;
        }

        /*
                public DomainEvent(DateTime createdAt)
                {
                    CreatedAt = createdAt;
                }
        */

        public DateTime CreatedAt { get; set; }
    }
}
