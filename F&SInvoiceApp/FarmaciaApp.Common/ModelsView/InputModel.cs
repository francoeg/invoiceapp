﻿namespace InvoiceApp.Common.ModelsView
{
    public class InputModel<T>
    {
        public T Id { get; set; }

        public string Name { get; set; }
    }
}
