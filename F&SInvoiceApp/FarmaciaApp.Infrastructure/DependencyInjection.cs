﻿using FarmaciaApp.Common.Interfaces;
using FarmaciaApp.Infrastructure.Configurations;
using FarmaciaApp.Infrastructure.Data;
using FarmaciaApp.Infrastructure.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace FarmaciaApp.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>(cfg =>
            {
                cfg.Tokens.AuthenticatorTokenProvider = TokenOptions.DefaultAuthenticatorProvider;
                cfg.SignIn.RequireConfirmedEmail = true;
                cfg.User.RequireUniqueEmail = true;
                cfg.Password.RequireDigit = false;
                cfg.Password.RequiredUniqueChars = 0;
                cfg.Password.RequireLowercase = false;
                cfg.Password.RequireNonAlphanumeric = false;
                cfg.Password.RequireUppercase = false;
                cfg.Password.RequiredLength = 5;
            })
                .AddDefaultTokenProviders()
             .AddEntityFrameworkStores<ApplicationDbContext>();


            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));

            services.AddTransient<IDateTime, MachineDateTime>();

            //services.AddDefaultIdentity<ApplicationUser>()
              //  .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddIdentityServer()
                .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

            //  services.AddAuthentication()
            //.AddIdentityServerJwt();


            services.AddAuthentication()
                  .AddJwtBearer(cfg =>
                  {
                      cfg.TokenValidationParameters = new TokenValidationParameters
                      {
                          ValidIssuer = configuration["Tokens:Issuer"],
                          ValidAudience = configuration["Tokens:Audience"],
                          IssuerSigningKey = new SymmetricSecurityKey(
                                                  Encoding.UTF8.GetBytes(configuration["Tokens:Key"]))
                      };
                  });

            return services;
        }
    }
}
