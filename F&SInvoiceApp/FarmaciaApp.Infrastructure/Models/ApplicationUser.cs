﻿using Microsoft.AspNetCore.Identity;

namespace FarmaciaApp.Infrastructure.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string NewID() {
            return new IdentityUser().Id;
        }
    }
}
