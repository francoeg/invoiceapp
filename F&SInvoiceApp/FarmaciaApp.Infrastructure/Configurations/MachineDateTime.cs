﻿using FarmaciaApp.Common.Interfaces;
using System;

namespace FarmaciaApp.Infrastructure.Configurations
{
    public class MachineDateTime : IDateTime
    {
        public DateTime Now => DateTime.Now;

        public int CurrentYear => DateTime.Now.Year;
    }
}
