﻿using FarmaciaApp.Application.Common.Interfaces;
using FarmaciaApp.Common.Interfaces;

using FarmaciaApp.Infrastructure.Models;
using IdentityServer4.EntityFramework.Options;
using Inovice.Domain.Common;
using Invoice.Domain.Entities;
using InvoiceApp.Common.Interface;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FarmaciaApp.Infrastructure.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>, IDbContext
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;

        public ApplicationDbContext(
        DbContextOptions options,
        IOptions<OperationalStoreOptions> operationalStoreOptions,
          ICurrentUserService currentUserService,
          IDateTime dateTime) : base(options, operationalStoreOptions)
    {
            _currentUserService = currentUserService;
            _dateTime = dateTime;
        }

        public DbSet<Setting> Settings { get; set; }
        public DbSet<Envelope> Envelopes { get ; set ; }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            //Todo:Configurar esto en el program
            foreach (var entry in ChangeTracker.Entries<AuditEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = _currentUserService.UserId;
                        entry.Entity.Created = _dateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = _currentUserService.UserId;
                        entry.Entity.LastModified = _dateTime.Now;
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }
      
    }
}
