﻿using FarmaciaApp.Common.Mappings;
using FarmaciaApp.Common.Models.Request;

namespace Users.Application.EntitiesVM
{
    public class InsertUserCommnad :  EntityRequest<UserVM>
    {
        public string Password { get; set; }
    }
}
