﻿using AutoMapper;
using FarmaciaApp.Infrastructure.Models;
using User.Application.EntitiesVM;

namespace User.Application.User.Command
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<ApplicationUser, UserVM>().ConstructUsing(i => new UserVM
            {
                Id = i.Id,
                Address = i.Address,
                FirstName = i.FirstName,
                LastName = i.LastName,
                PhoneNumber = i.PhoneNumber,
                Username = i.UserName,
                Password="xxxxxx"
                
            });

            CreateMap<UserVM, ApplicationUser>().ConstructUsing(i => new ApplicationUser
            {
                Id = i.Id,
                Address = i.Address,
                FirstName = i.FirstName,
                LastName = i.LastName,
                PasswordHash = i.Password,
                PhoneNumber = i.PhoneNumber,
                UserName = i.Username,
                Email = i.Username
            });
        }
    }
}
