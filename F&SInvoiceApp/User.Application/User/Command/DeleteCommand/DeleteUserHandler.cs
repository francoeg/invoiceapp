﻿
using FarmaciaApp.Common.Commands.BaseRequest;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models.Result;
using FarmaciaApp.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using User.Application.Services;

namespace User.Application.User.Command.DeleteCommand
{
    public class DeleteUserHandler : BaseRequestHandler<DeleteUserRequest, Result>
    {
        private readonly IUserManager _userManager;

        public DeleteUserHandler(IEventMediator eventPublisher,
                                 IUserManager  userManager ) : base(eventPublisher)
        {
            this._userManager = userManager;
        }

        public override async  Task<Result> Handle(DeleteUserRequest request, CancellationToken cancellationToken)
        {

            ApplicationUser user = await _userManager.GetUserByIdAsync(request.Id);
            request.AddNotifications(Exists(user));

       
             await _userManager.DeleteUserAsync(user);

             await EventPublisher.Publish<UserDelete>
                                (new UserDelete(request.Id),cancellationToken);
            
            return new Result(request.Notifications);
        }
    }
}
