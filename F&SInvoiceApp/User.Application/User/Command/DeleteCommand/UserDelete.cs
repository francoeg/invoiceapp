﻿using FarmaciaApp.Common.Commands.Command;
using FarmaciaApp.Common.Commands.Mediator;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace User.Application.User.Command.DeleteCommand
{
    public class UserDelete : DomainEvent
    {
        public UserDelete(string Id)
        {
            this.Id = Id;
        }

        public string Id { get; set; }

  
    }
    public class UserDeletedHandler : INotificationHandler<UserDelete>
    {
        //private readonly IEventMediator _eventPublisher;
        //private readonly INotificationService _notification;

        //public UserDeletedHandler(IEventMediator eventPublisher,
        //                          INotificationService notification)
        //{
        //    //this._eventPublisher = eventPublisher;
        //    //this._notification = notification;
        //}

        public Task Handle(UserDelete notification, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                Console.WriteLine($"User Delete: '{notification.Id} - {notification.CreatedAt}");
            });
        }
    }
}
