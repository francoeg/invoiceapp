﻿using FarmaciaApp.Common.Models.Request;
using FarmaciaApp.Common.Validation;
using FluentValidation;

namespace User.Application.User.Command.DeleteCommand
{
    public class DeleteUserRequest : CommandRequest
    {
        public DeleteUserRequest(string id)
        {
            this.Id = id;

            AddNotifications(new NotEmptyValidator<string>().Validate(id));
            ///  AddNotifications(new NotNullValidator<int>().Validate(this));
        }

        public string Id { get; }
    }
}
