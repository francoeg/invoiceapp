﻿using FluentValidation;

namespace User.Application.User.Command.InsertCommand
{
    public class InsertUsertRequestValidator : AbstractValidator<InsertUserRequest>
    {
        public InsertUsertRequestValidator()
        {
            RuleFor(x => x.Item).NotNull();
            
         //  RuleFor(x => x.Item.FirstName).NotEmpty();
            //RuleFor(x => x.Item.LastName).NotEmpty();
           // RuleFor(x => x.Item.PhoneNumber).NotEmpty();
            RuleFor(x => x.Item.Username).NotEmpty().EmailAddress();
            RuleFor(x => x.Item.Password).NotEmpty().MinimumLength(8).MaximumLength(16);
            RuleFor(x => x.Item.ConfirmPassword).NotEmpty().MinimumLength(8).MaximumLength(16);
            RuleFor(x => x.Item.Password).Equal(x => x.Item.ConfirmPassword);
        }
    }
}
