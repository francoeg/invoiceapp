﻿using FarmaciaApp.Common.Models.Request;
using User.Application.EntitiesVM;

namespace User.Application.User.Command.InsertCommand
{
    public class InsertUserRequest : EntityRequest<UserVM>
    {
        public InsertUserRequest(UserVM item): base(item)
        {
            AddNotificationsValidation(new InsertUsertRequestValidator().Validate(this));
        }

      
    }
}
