﻿using AutoMapper;
using FarmaciaApp.Common.Commands.BaseRequest;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using FarmaciaApp.Infrastructure.Models;
using System.Threading;
using System.Threading.Tasks;
using User.Application.EntitiesVM;
using User.Application.Services;

namespace User.Application.User.Command.InsertCommand
{
    public class UserInsertHandler : BaseRequestHandler<InsertUserRequest, EntityResult<UserVM>>
    {
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;

        public UserInsertHandler(IEventMediator eventPublisher,
                                 IUserManager userManager,
                                 IMapper mapper) : base(eventPublisher)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public override async Task<EntityResult<UserVM>> Handle(InsertUserRequest request, CancellationToken cancellationToken)
        {
 
                ApplicationUser useraplication = _mapper.Map<ApplicationUser>                                                                        (request.Item);

                if (useraplication != null)
                {
                   _ = await _userManager.AddUserAsync(useraplication, request.Item.Password);
                }
 
            return new EntityResult<UserVM>(request.Notifications, request.Item);
        }
    }
}
