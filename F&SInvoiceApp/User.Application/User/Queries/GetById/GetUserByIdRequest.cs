﻿using FarmaciaApp.Common.Models.Request;
using FarmaciaApp.Common.Validation;
using User.Application.EntitiesVM;

namespace User.Application.User.Queries.GetById
{
    public class GetUserByIdRequest : EntityRequest<UserVM>
    {
        public GetUserByIdRequest(UserGetByIdVm UserGetByIdVm) : base(new UserVM())
        {
            UserGetById = UserGetByIdVm;

              AddNotificationsValidation(new NotEmptyValidator<string>().Validate(UserGetById.Id));
        }
        public UserGetByIdVm UserGetById { get; }
    }
}
