﻿using AutoMapper;
using FarmaciaApp.Common.Commands.BaseRequest;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using FarmaciaApp.Infrastructure.Models;
using System.Threading;
using System.Threading.Tasks;
using User.Application.EntitiesVM;
using User.Application.Services;

namespace User.Application.User.Queries.GetById
{
    public class GetUserByIdHandler : BaseRequestHandler<GetUserByIdRequest,EntityResult<UserVM>>
    {
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;

        public GetUserByIdHandler(IEventMediator eventPublisher,
                                  IUserManager userManager,
                                   IMapper mapper) : base(eventPublisher)
        {
            this._userManager = userManager;
            this._mapper = mapper;
        }

        public override async Task<EntityResult<UserVM>> Handle(GetUserByIdRequest  request, 
                                                                CancellationToken  cancellationToken)
        {
            ApplicationUser user =await _userManager.GetUserByIdAsync(request.UserGetById.Id);
            
            request.AddNotifications(Exists(user));
       
            UserVM userVm = _mapper.Map<UserVM>(user);

            return GenerateEntityResult(request.Notifications, userVm);   
        }
    }
}
