﻿using FarmaciaApp.Common.Models.Request;
using User.Application.EntitiesVM;

namespace User.Application.User.Queries.GetAll
{
    public class FindUserRequest : QueryRequest<UserVM>
    {
        public FindUserRequest(UserFindVm item, 
                               int pageIndex, 
                               int pageSize ) 
                        : base(pageIndex,
                               pageSize)
        {
            Item = item;
            //TODO: agregar Validator
        }

        public UserFindVm Item { get; set; }
    }
}
