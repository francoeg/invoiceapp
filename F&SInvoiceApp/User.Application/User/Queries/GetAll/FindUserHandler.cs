﻿using FarmaciaApp.Common.Commands.BaseRequest;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using FarmaciaApp.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using User.Application.EntitiesVM;
using User.Application.Services;
using System.Linq;
using AutoMapper;

namespace User.Application.User.Queries.GetAll
{
    public class FindUserHandler : BaseRequestHandler<FindUserRequest,
                                                      QueryResult<UserVM>>
    {
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;

        public FindUserHandler(IEventMediator eventPublisher,
                               IUserManager userManager,
                               IMapper mapper)    
                        : base(eventPublisher)
        {
            this._userManager = userManager;
            this._mapper = mapper;
        }

        public override  Task<QueryResult<UserVM>> Handle(FindUserRequest request,
                                                          CancellationToken cancellationToken)
        {

            Tuple<IEnumerable<ApplicationUser>, long> list =
                                            this._userManager.FindAsync(request.Item.Id,
                                                                   request.Item.UserName,
                                                                   request.PageIndex,
                                                                   request.PageSize);

            IEnumerable<UserVM> listUserVm = list.Item1.Select(x=>
                                                               _mapper.Map<UserVM>(x));

            return Task.FromResult( new QueryResult<UserVM>(request.Notifications,
                                                            listUserVm,
                                                            list.Item2));
        }
    }
}
