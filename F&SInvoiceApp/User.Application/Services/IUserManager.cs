﻿
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserWeb = FarmaciaApp.Infrastructure.Models.ApplicationUser;

namespace User.Application.Services
{
    public interface IUserManager
    {
        Tuple<IEnumerable<UserWeb>, long> FindAsync(string id,
                                                    string UserName,
                                                    int pageIndex,
                                                    int pageSize);
        Task<UserWeb> GetUserByEmailAsync(string email);

        Task<IdentityResult> AddUserAsync(UserWeb user, string password);

        Task<IdentityResult> UpdateUserAsync(UserWeb user);

        Task<IdentityResult> ChangePasswordAsync(UserWeb user, string oldPassword, string newPassword);

        Task<bool> ValidatePasswordAsync(UserWeb user, string password);

        Task CheckRoleAsync(string roleName);

        Task AddUserToRoleAsync(UserWeb user, string roleName);

        Task<bool> IsUserInRoleAsync(UserWeb user, string roleName);

        Task<string> GenerateEmailConfirmationTokenAsync(UserWeb user);

        Task<IdentityResult> ConfirmEmailAsync(UserWeb user, string token);

        Task<UserWeb> GetUserByIdAsync(string userId);

        Task<string> GeneratePasswordResetTokenAsync(UserWeb user);

        Task<IdentityResult> ResetPasswordAsync(UserWeb user, string token, string password);

        Task<List<UserWeb>> GetAllUsersAsync();

        Task RemoveUserFromRoleAsync(UserWeb user, string roleName);

        Task DeleteUserAsync(UserWeb user);
        public string GenerateToken(UserWeb user);

        Task<UserWeb> GetUserByUserName(string userName);

         Task<bool> AddToRole(string Name);

        IEnumerable<IdentityRole> GetRoles(string Name, int pageIndex, int pageSize);
    }
}
