﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using UserWeb = FarmaciaApp.Infrastructure.Models.ApplicationUser;

namespace User.Application.Services
{
    public class UserManagerService : IUserManager
    {
        private readonly UserManager<UserWeb> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration configuration;

        public UserManagerService(UserManager<UserWeb> userManager,
                                  RoleManager<IdentityRole> roleManager,
                                  IConfiguration configuration)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.configuration = configuration;
        }

        public Tuple<IEnumerable<UserWeb>, long> FindAsync(string id, 
                                                           string userName,
                                                           int pageIndex,
                                                           int pageSize) 
       {
            bool Predicate(UserWeb x) =>
                    (string.IsNullOrEmpty(id) || x.Id == id) &&
                    (string.IsNullOrEmpty(userName) || x.UserName.Contains(userName));

            List<UserWeb> listUSer = this.userManager.Users
                                      .Where(Predicate)
                                      .Skip((pageIndex - 1) * pageSize)
                                      .Take(pageSize)
                                      .OrderBy(x=>x.UserName)
                                      .ToList();

            int count = listUSer.Count();

            return (new Tuple<IEnumerable<UserWeb>, long>(listUSer, count));
        }

        public async Task<IdentityResult> AddUserAsync(UserWeb user, string password)
        {
            if (user.Id == null|| user.Id=="")
            {
                user.Id = new UserWeb().Id;
            }
            return await userManager.CreateAsync(user, password);
        }

        public async Task<IdentityResult> ChangePasswordAsync(UserWeb user, string oldPassword, string newPassword)
        {
            return await userManager.ChangePasswordAsync(user, oldPassword, newPassword);
        }

        public async Task<UserWeb> GetUserByEmailAsync(string email)
        {
            UserWeb user = await userManager.FindByEmailAsync(email);
            return user;
        }

        public async Task<UserWeb> GetUserByUserName(string userName)
        {
            return await userManager.FindByNameAsync(userName);
        }

        public async Task CheckRoleAsync(string roleName)
        {
            bool roleExists = await roleManager.RoleExistsAsync(roleName);
            if (!roleExists)
            {
                await roleManager.CreateAsync(new IdentityRole
                {
                    Name = roleName
                });
            }
        }

        public async Task AddUserToRoleAsync(UserWeb user, string roleName)
        {
            await userManager.AddToRoleAsync(user, roleName);
        }

        public async Task<bool> IsUserInRoleAsync(UserWeb user, string roleName)
        {
            return await userManager.IsInRoleAsync(user, roleName);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(UserWeb user, string token)
        {
            return await userManager.ConfirmEmailAsync(user, token);
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(UserWeb user)
        {
            return await userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<UserWeb> GetUserByIdAsync(string userId)
        {
            return await userManager.FindByIdAsync(userId);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(UserWeb user)
        {
            return await userManager.GeneratePasswordResetTokenAsync(user);
        }

        public async Task<IdentityResult> ResetPasswordAsync(UserWeb user, string token, string password)
        {
            return await userManager.ResetPasswordAsync(user, token, password);
        }

        public async Task<List<UserWeb>> GetAllUsersAsync()
        {
            return await userManager.Users
                 .OrderBy(u => u.FirstName)
                .ThenBy(u => u.LastName)
                .ToListAsync();
        }

        public async Task RemoveUserFromRoleAsync(UserWeb user, string roleName)
        {
            await userManager.RemoveFromRoleAsync(user, roleName);
        }

        public async Task DeleteUserAsync(UserWeb user)
        {
            await userManager.DeleteAsync(user);
        }

        public string GenerateToken(UserWeb user)
        {

            string keyString = configuration["Tokens:Key"];
            string isssuer = configuration["Tokens:Issuer"];
            string audience = configuration["Tokens:Audience"];

            Claim[] claims = new[]{
                                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                             };

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(keyString));
            SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            JwtSecurityToken token = new JwtSecurityToken(
                                                isssuer,
                                                audience,
                                                claims,
                                                notBefore: DateTime.Now,
                                                expires: DateTime.UtcNow.AddHours(8),
                                                signingCredentials: credentials);

            string tokenHandler = new JwtSecurityTokenHandler().WriteToken(token);

            return tokenHandler;
        }

        public Task<IdentityResult> UpdateUserAsync(UserWeb user)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> ValidatePasswordAsync(UserWeb user,
                                                              string password)
        {
            return await this.userManager.CheckPasswordAsync(user,
                                                             password);
        }
        public async Task<bool> AddToRole(string Name)
        {

            IdentityResult role = await roleManager.CreateAsync
                                                    (new IdentityRole { Name = Name });
           
            return role.Succeeded;
        }

        public IEnumerable<IdentityRole> GetRoles(string Name,
                                                           int pageIndex,
                                                           int pageSize)
        {
            bool Predicate(IdentityRole x) =>
                  (string.IsNullOrEmpty(Name) || x.Name.Contains(Name));

            IEnumerable<IdentityRole> roles =  roleManager.Roles
                                              .Where(Predicate)
                                              .Skip((pageIndex - 1) * pageSize)
                                              .Take(pageSize)
                                              .OrderBy(x => x.Name)
                                              .ToList();
            return  roles;
        }
    }
}
