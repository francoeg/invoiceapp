﻿using AutoMapper;
using FarmaciaApp.Common.Commands.BaseRequest;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using System.Threading;
using System.Threading.Tasks;
using User.Application.EntitiesVM;
using User.Application.Services;
using UserWeb = FarmaciaApp.Infrastructure.Models.ApplicationUser;

namespace User.Application.UserLogin.Command.Login
{
    public class UserLoginHandler : BaseRequestHandler<UserLoginRequest, EntityResult<UserTokenVm>>
    {
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;

        public UserLoginHandler(IEventMediator eventPublisher,
                                IUserManager userManager,
                                IMapper mapper) : base(eventPublisher)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public override async Task<EntityResult<UserTokenVm>> Handle(UserLoginRequest request, CancellationToken cancellationToken)
        {
            UserWeb user = await _userManager.GetUserByUserName(request.ItemR.UserName);

            if (user == null) request.AddNotificationsValidation(Error("No se encontro Usuario."));
                
                bool response = await _userManager.ValidatePasswordAsync(user,                                                                 request.ItemR.Password);
          
            if (!response) request.AddNotificationsValidation(Error("Contraseña Incorrecta."));

             UserTokenVm userTokenVm = _mapper.Map<UserTokenVm>(user);
             userTokenVm.Token = _userManager.GenerateToken(user);
                
            return GenerateEntityResult(request.Notifications, userTokenVm);  
        }
    }
}
