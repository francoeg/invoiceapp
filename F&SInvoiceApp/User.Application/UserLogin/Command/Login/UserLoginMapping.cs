﻿using AutoMapper;
using FarmaciaApp.Infrastructure.Models;
using User.Application.EntitiesVM;

namespace User.Application.UserLogin.Command.Login
{
    internal class UserLoginMapping : Profile
    {
        public UserLoginMapping()
        {
            CreateMap<UserTokenVm, ApplicationUser>().ReverseMap();

            CreateMap<ApplicationUser, UserTokenVm>().ConvertUsing(
                                            i=> new UserTokenVm
                                            {
                                             Username  = i.UserName,
                                             FirstName = i.FirstName,
                                             LastName  = i.LastName
                                            }); 
        }
    }
}
