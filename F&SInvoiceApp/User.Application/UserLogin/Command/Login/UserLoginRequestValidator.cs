﻿using FluentValidation;

namespace User.Application.UserLogin.Command.Login
{
    public class UserLoginRequestValidator : AbstractValidator<UserLoginRequest>
    {
        public UserLoginRequestValidator()
        {
            RuleFor(x => x.ItemR).NotNull();
            RuleFor(x => x.ItemR.UserName).NotNull().NotEmpty();
            RuleFor(x => x.ItemR.Password).NotNull().NotEmpty().MinimumLength(6);
        }
    }
}
