﻿using FarmaciaApp.Common.Models.Request;
using User.Application.EntitiesVM;

namespace User.Application.UserLogin.Command.Login
{
    public class UserLoginRequest : EntityRequest<UserTokenVm>
    {
        public UserLoginRequest(UserLoginVM item) : base (new UserTokenVm())
        {
            ItemR = item;
            AddNotificationsValidation(new UserLoginRequestValidator().Validate(this));
         }

        public UserLoginVM ItemR { get; }
    }
}
