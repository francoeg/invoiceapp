﻿using System.Collections.Generic;

namespace User.Application.Models
{
     public class Menu
    {
        public Menu()
        {
            this.SubMenues = new List<SubMenu>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public List<SubMenu> SubMenues { get; set; }
    }

    public class SubMenu
    {
        public int Id { get; set; }

        public int IdMenu { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public string Icon { get; set; }

    }
}
