﻿
namespace User.Application.ModelDto
{
    public class RoleDto
    {
        public RoleDto(string name) => Name = name; 
       
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
