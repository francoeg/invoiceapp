﻿
using FarmaciaApp.Common.Models.Request;
using User.Application.EntitiesVM.Find;
using User.Application.ModelDto;

namespace User.Application.Roles.Queries.GetAll
{
    public class RoleFindRequest : QueryRequest<RoleDto>
    {
        public RoleFindRequest(RoleFindVM item,
                               int pageIndex,
                               int pageSize)
                        : base(pageIndex, pageSize)
        {
            this.Item = item;
        }


        public RoleFindVM Item { get; set; }
    }
}
