﻿
using AutoMapper;
using FarmaciaApp.Common.Commands.BaseRequest;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using User.Application.ModelDto;
using User.Application.Services;

namespace User.Application.Roles.Queries.GetAll
{
    public class RoleFindHandler : BaseRequestHandler<RoleFindRequest,
                                                      QueryResult<RoleDto>>
    {
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;

        public RoleFindHandler(IUserManager userManager,
                               IMapper mapper,
                               IEventMediator eventPublisher) : base(eventPublisher)
        {
            this._userManager = userManager;
            this._mapper = mapper;
        }

        public override Task<QueryResult<RoleDto>> Handle(RoleFindRequest request, CancellationToken cancellationToken)
        {
            IEnumerable<RoleDto> rolesList = this._userManager.GetRoles(request.Item.Name,request.PageIndex,request.PageSize).Select(x=>_mapper.Map<RoleDto>(x));

            return Task.FromResult(new QueryResult<RoleDto>(request.Notifications,
                                                           rolesList,
                                                           rolesList.Count()));
        }
    }
}
