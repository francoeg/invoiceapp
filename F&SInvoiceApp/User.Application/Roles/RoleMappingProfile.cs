﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using User.Application.ModelDto;

namespace User.Application.Roles
{
    public class RoleMappingProfile : Profile
    {
        public RoleMappingProfile()
        {
            CreateMap<RoleDto, IdentityRole>();

            CreateMap< IdentityRole , RoleDto>();
        }
    }
}
