﻿using FluentValidation;


namespace User.Application.Roles.Command.InsertCommnad
{
    public class RoleInsertRequestValidators : AbstractValidator<RoleInsertRequest>
    {
        public RoleInsertRequestValidators()
        {
            RuleFor(x => x.Item.Name).NotEmpty()
                                     .NotEqual(string.Empty)
                                     .MinimumLength(2)
                                     .WithMessage("El Role debe ser mayor a dos Caracteres.");

        }
    }
}
