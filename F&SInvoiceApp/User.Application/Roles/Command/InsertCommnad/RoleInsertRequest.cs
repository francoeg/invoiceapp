﻿using FarmaciaApp.Common.Models.Request;
using User.Application.ModelDto;

namespace User.Application.Roles.Command.InsertCommnad
{
    public class RoleInsertRequest : EntityRequest<RoleDto>
    {
        public RoleInsertRequest(RoleDto item): base(item)
        {
            AddNotificationsValidation(new RoleInsertRequestValidators().Validate(this));
        }
   }
}
