﻿using FarmaciaApp.Common.Commands.BaseRequest;
using FarmaciaApp.Common.Commands.Mediator;
using FarmaciaApp.Common.Models;
using Microsoft.AspNetCore.Identity;
using System.Threading;
using System.Threading.Tasks;
using User.Application.ModelDto;
using User.Application.Services;

namespace User.Application.Roles.Command.InsertCommnad
{
    public class RoleInsertHandler : BaseRequestHandler<RoleInsertRequest, EntityResult<RoleDto>>
    {
        private readonly IUserManager _userManager;

        public RoleInsertHandler(IEventMediator eventPublisher,
                                 IUserManager userManager
                                 ) : base(eventPublisher)
        {
            this._userManager = userManager;
        }

        public override async  Task<EntityResult<RoleDto>> Handle(RoleInsertRequest request, CancellationToken cancellationToken)
        {

            await _userManager.AddToRole(request.Item.Name);


            return new EntityResult<RoleDto>(request.Notifications, request.Item);

        }
    }
}
