﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using User.Application.Services;
using User.Application.User.Command.InsertCommand;

namespace User.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddUserApplication(this IServiceCollection services)
        {
            //services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            //services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddMediatR(typeof(UserInsertHandler));
            services.AddTransient<IUserManager,UserManagerService>();
            return services;
        }
    }
}
