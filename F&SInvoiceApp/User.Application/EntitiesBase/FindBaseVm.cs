﻿namespace User.Application.EntitiesBase
{
    public class FindBaseVm<T>
    {
        public T Id { get; set; }
      
        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string Name { get; set; }
    }
}
