﻿namespace User.Application.EntitiesVM
{
    public class UserGetByIdVm
    {
        public string Id { get; set; }

        public string UserName { get; set; }
    }
}
