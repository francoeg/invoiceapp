﻿using User.Application.EntitiesBase;

namespace User.Application.EntitiesVM
{
    public class UserFindVm : FindBaseVm<string>
    {
        public string UserName { get; set; }
    }
}
