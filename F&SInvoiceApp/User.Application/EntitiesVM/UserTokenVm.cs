﻿using User.Application.EntitiesBase;

namespace User.Application.EntitiesVM
{
    public class UserTokenVm : UserBase
    {
        public string Token { get; set; }
    }
}
