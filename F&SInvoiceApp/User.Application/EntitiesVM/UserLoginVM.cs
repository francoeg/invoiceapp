﻿namespace User.Application.EntitiesVM
{
    public class UserLoginVM
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public bool Remember { get; set; }
    }
}
