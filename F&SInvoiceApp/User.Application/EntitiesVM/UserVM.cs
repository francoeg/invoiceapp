﻿using User.Application.EntitiesBase;

namespace User.Application.EntitiesVM
{
    public class UserVM  : UserBase
    {
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }


        //public void FormatUserVM() {
        //    this.Address = string.Empty;
        //    this.Confirm = string.Empty;
        //    this.Id = string.Empty;
        //    this.Password = string.Empty;
        //    this.PhoneNumber = string.Empty;
        //}
    }
}
