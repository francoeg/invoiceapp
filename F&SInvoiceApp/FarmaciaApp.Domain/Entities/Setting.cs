﻿using Invoice.Domain.Interfaces;

namespace Invoice.Domain.Entities
{
    public class Setting : IEntity<int>
    {
        public int Id{ get ; set ; }       
        public string Name { get; set; }
        public string Value { get; set; }
        public double ValueNumber { get; set; }
    }
}
