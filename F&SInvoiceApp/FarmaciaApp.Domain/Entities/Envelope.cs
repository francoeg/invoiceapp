﻿using Invoice.Domain.Interfaces;

namespace Invoice.Domain.Entities
{
    public class Envelope : IEntity<int>
    {
        public int Id { get; set ; }

        public string Title { get; set; }

        public string Header { get; set; }

        public string Footer { get; set; }

        public string Details { get; set; }
    }
}
